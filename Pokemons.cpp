#include "Pokemons.h"
#include <iostream>
#include <string>
Pokemon::Pokemon(string name, string eleType, int hp, int speed, int level, int dmg)	
{
	Pokemon* Bulbasaur = new Pokemon("Bulbasaur", "Grass Type", 45,45,1,49);
	Pokemon* Cyndaquil = new Pokemon("Cyndauil", "Fire Type", 39,65,1,52);
	Pokemon* Totodile = new Pokemon("Totodile", "Water Type", 50,43,1,65);
	Pokemon* Charmander = new Pokemon("Charmander", "Fire Type", 39,65,1,52);
	Pokemon* Squirtle = new Pokemon("Squirtle", "Water Type", 44,43,1,44);
	Pokemon* Meowth = new Pokemon("Meowth", "Normal Type", 40,90,1,45);
	Pokemon* Ponyta = new Pokemon("Ponyta", "Fire Type", 50,90,1,85);
	Pokemon* Pikachu = new Pokemon("Pikachu", "Thunder Type", 35,90,1,55);
	Pokemon* Poliwag = new Pokemon("Poliwag", "Water Type", 40,90,1,50);
	Pokemon* Chikorita = new Pokemon("Chikorita", "Grass Type", 45,45,1,49);
	Pokemon* Ratata = new Pokemon("Ratata", "Normal Type", 30,72,1,56);
	Pokemon* Spearrow = new Pokemon("Spearrow", "Normal type", 40,70,1,60);
	Pokemon* Vulpix = new Pokemon("Vulpix", "Fire Type", 38,65,1,41);
	Pokemon* Tangela = new Pokemon("Tangela", "Grass Type", 65,60,1,55);
	Pokemon* Voltorb = new Pokemon("Voltorb", "Thunder Type", 40,100,1,30);
}

void Pokemon::pokemonStats()
{
	cout << this->name << "\nStats";
	cout << "Base Hp:" << this->baseHp << endl;
	cout << "Current Hp: " << this->hp << endl;
	cout << "Level: " << this->level << endl;
	cout << "base Damage: " << this->baseDmg << endl;
	cout << "Exp: " << this->exp << endl;
	cout << "Exp to next level: " << this->expToNextlvl << endl;
}
