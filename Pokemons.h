#pragma once
#include <string>

using namespace std;

class Pokemon
{
public:

	
	Pokemon(string name, string eleType,int hp,int speed,int level,int dmg);

	string name;
	int baseHp;
	int hp;
	int level;
	int baseDmg;
	int speed;
	int exp;
	int expToNextlvl;
	string eleType;

	void pokemonStats();
	

};